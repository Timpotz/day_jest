const request = require('supertest');
const app = require('../server');

describe('Root Path', () => {
  describe('GET /', () => {

    test('Should return 200', done => {
      request(app).get('/')
        .then(res => {
          expect(res.statusCode).toEqual(200);
          expect(res.body.status).toEqual('success');
          expect(res.body.message).toEqual('Hello World');
          done();
        })
    })

  })
})
