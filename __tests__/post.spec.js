const request = require('supertest');
const app = require('../server');
const { Post } = require('../models');

describe('Posts API Collection', () => {
  
  beforeAll(() => {
    return Post.destroy({
      truncate: true
    })
  })

  afterAll(() => {
    return Post.destroy({
      truncate: true
    })
  })

  describe('POST /api/v1/posts', () => {

    test('Should succesfully create new post', done => {
      request(app)
        .post('/api/v1/posts')
        .set('Content-Type', 'application/json')
        .send({ title: 'Hello World', body: 'Lorem Ipsum' })
        .then(res => {
          expect(res.statusCode).toEqual(201);
          expect(res.body.status).toEqual('success');

          expect(res.body.data.post)
            .toEqual(
              expect.objectContaining({
                id: expect.any(Number),
                title: expect.any(String),
                body: expect.any(String),
              })
            );
          done();
        })
    })

    test('Should not create new post', done => {
      request(app)
        .post('/api/v1/posts')
        .set('Content-Type', 'application/json')
        .send({ body: 'Lorem Ipsum' })
        .then(res => {
          expect(res.statusCode).toEqual(422);
          expect(res.body.status).toEqual('fail');
          done();
        })
    })

  })
})
