const router = require('express').Router();

// Import controller
const index = require('./controllers/indexController');
const post = require('./controllers/postController.js');

router.get('/', index.home);

// Post Collection API
router.post('/api/v1/posts', post.create);
router.get('/api/v1/posts', post.index);

module.exports = router;
