// Import on top
const express = require('express');
const morgan = require('morgan');
require('dotenv').config();

const router = require('./router');

// Instantiate Express
const app = express();

// Basic Express Setup
app.use(express.json());
if (process.env.NODE_ENV !== 'test')
  app.use(morgan('dev'))
app.use(router);

/*
 * Export app
 * Because we want to use it for
 * Two different purposes
 * */
module.exports = app;
